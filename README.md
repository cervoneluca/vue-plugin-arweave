# Vue Arweave

A Vue Plugin that supplies reactive functionalities that can be used in Vue components and templates and also wraps the functionalities exposed by the [ArweaveJS](https://github.com/ArweaveTeam/arweave-js) library.

<div style="text-align:center; width:100%">
    <img align="center" src="https://gitlab.com/cervoneluca/vue-plugin-arweave/-/raw/master/test/assets/logo.png" alt="vue-logo" title="vue-logo" height="100">
    <img align="center" src="https://gitlab.com/cervoneluca/vue-plugin-arweave/-/raw/master/test/assets/ar-logo.svg" alt="arweave-logo" title="arweave-logo" height="100">
</div>

<h2 style="font-size: 25px; font-weight:bold">
NOTE: this plugin is funded by means of a <a href="https://gitcoin.co/grants/978/vuearweave-grant">GitCoin Grant</a>. If you think that the plugin is useful, and if you would like that I continue to develop it by adding other functionalities (for instance functionalities related to Arweave Smart Contracts), support the developing with some DAI <a href="https://gitcoin.co/grants/978/vuearweave-grant">https://gitcoin.co/grants/978/vuearweave-grant</a>! 💪🦾🖖❤️
</h2>

# Table of Contents

<!-- toc -->

- [Installation and Setup](#installation-and-setup)
- [Wallets](#wallets)
- [Transactions](#transactions)
- [Utility Methods](#utility-methods)
- [Full API](#full-api)
  * [Methods](#methods)
    + [$arGenerateWallet](#argeneratewallet)
    + [$arImportWallet](#arimportwallet)
    + [$arRemoveWallet](#arremovewallet)
    + [$arSetDefaultWallet](#arsetdefaultwallet)
    + [$arCreateTransaction](#arcreatetransaction)
    + [$arUtilsSignTransaction](#arutilssigntransaction)
    + [$arUtilsSendTransaction](#arutilssendtransaction)
    + [$arUtilsExecArQL](#arutilsexecarql)
    + [$arUtilsGetTransactionsForAddress](#arutilsgettransactionsforaddress)
    + [$arUtilsOwnerToAddres](#arutilsownertoaddres)
    + [$arUtilsWinstonToAR](#arutilswinstontoar)
    + [$arUtilsGetTransaction](#arutilsgettransaction)
    + [$arUtilsGetTransactionStatus](#arutilsgettransactionstatus)
    + [$arUtilsEnrichTransaction](#arutilsenrichtransaction)
    + [$arUtilsTransactionGetData](#arutilstransactiongetdata)
  * [Computed Properties](#computed-properties)
    + [$arGetWallets](#argetwallets)
    + [$arGetDefaultWallet](#argetdefaultwallet)
    + [$arGetARTransactionsQueue](#argetartransactionsqueue)
    + [$arGetDefaultWalletTransactionHistory](#argetdefaultwallettransactionhistory)
- [Compile and minify for production](#compile-and-minify-for-production)
- [Test](#test)

<!-- tocstop -->

## Installation 

<img align="center" src="https://gitlab.com/cervoneluca/openbits/-/raw/master/assets/logo-black.png" height="50px" alt="OpenBits logo" title="OpenBits Logo"> By starting by version 2.0.4 this package is published on <a href="http://openbits.world" target="_blank">OpenBits</a>.  

To install OpenBits run: 

```shell
npm install openbits -g
```

Then login into openbits by following these <a href="https://www.npmjs.com/package/openbits" target="_blank">instructions</a>.

When OpenBits is set up, you can install vue-arweave as followings: 

```shell
openbits install vue-arweave
\\ or 
openbits install vue-arweave@2.0.4
```

> **OpenBits is a network that helps developers to earn from creating and using open source packages. Have a look to it here: <a href="http://openbits.world" target="_blank">http://openbits.world</a>.**

However, if you want to install the package by using NPM run: 

```bash
npm install vue-arweave
```

## Usage

Use the plugin in your Vue app: 

```javascript
import Vue from 'vue';
import VueArweave from 'vue-arweave';

Vue.use(VueArweave);
```

If you do not specify a network, the Arweave node will connect to the localhost node (if it detects it), or to the default Arweave host, that is the following: 

```javascript
{
    host: 'arweave.net',
    port: 443,
    protocol: 'https',
    timeout: 20000,
    logging: false,
}
```

If you want to use your custom network you have to specify the network options for the plugin as the following:

```javascript
Vue.use(VueArweave, {
    network: {
        host: 'yourhost',
        port: yourport,
        protocol: 'yourprotocol',
        timeout: yourtimeout,
        logging: false|true,
    }
});
```

After having installed and imported the plugin, the arweave.js 
library will be available in the Vue instance and you can access by using `vue.$arweave.node;` in your Vue Components

You can also use all the reactive functions and variable that the plugin supplies, as described in the following sections.

## Wallets

The basic idea is that applications can handle multiple wallets at once, but they can use only one wallet as default in a specific time. 

Thus, the plugin exposes a reactive array that contains all the wallets used by the application that can be accessed in components with the following: 

```javascript
this.$arGetWallets;
```

It can also be accessed in templates with the following: 

```html
<template>
    <div>
    {{$arGetWallets}}
    </div>
</template>
```

Wallets used by the plugins, and so wallets generated by means of the plugin or imported by means of the plugin, are objects that contain the following properties: 

```javascript
{
    id, // a unique id for the wallet (generated by the plugin)
    address, // the address of the wallet
    type, // 'generated' | 'imported' (generated if the wallet was generated by means of the plugin, imported if the wallet was imported by means of theplugin)
    name, // a name for the wallet, it can be specified when the wallet is generated or imported
    isDefault, // true if the wallet is the default wallet currently used by the plugin
    balanceAR, // the balance in AR of the wallet
    balanceWinston, // the balance in winston of the wallet
    jwk, // the json wallet key of the wallet
}
```

You can generate new wallets by using the following (in your vue components): 

```javascript
this.$arGenerateWallet({
    name: 'the name for your wallet', // not mandatory, the default is 'null'
    isDefault: false||true, // not mandatory, the default is false. 
})
```

If a wallet is the first one that is generated, or that is imported in the application, it will be set automatically as the default one. 

You can import a wallet by using the followings (in your vue components):

```javascript
this.$arImportWallet({
    name: 'the name for your wallet', // not mandatory, the default is 'null'
    isDefault: false||true, // not mandatory, the default is false. 
    jwt: 'your json wallet object', // mandatory, throws an error if the jwt is not specified
})
```

If you try to import the same wallet twice, the plugin will throw an error. 

You can remove a wallet from the wallets array by using the following (in your vue components): 

```javascript
this.$arRemoveWallet(
    walletId // the id of the wallet that you want to remove (that is the unique id generated by the plugins for the wallet when you have generated or imported it)
)
```

You can set a wallet as the default one for the application by running the following (in your vue components):

```javascript
this.$arSetDefaultWallet(
    walletId // the id of the wallet that you want to remove (that is the unique id generated by the plugins for the wallet when you have generated or imported it)
)
```

The default wallet can be accessed by means of the following (in your vue components):

```javascript
this.$arGetDefaultWallet
```

Also, the plugin supplies a reactive variable that contains all the transaction made by the default wallet. So, after having set the default wallet, you can access the following (in your vue components)

```javascript
this.$arGetDefaultWalletTransactionHistory
```

You can also access it your templates, as the following: 

```html
<template>
    <div>
    {{$arGetDefaultWalletTransactionHistory}}
    </div>
</template>
```

After having imported or generated at least one wallet, and so after having a defaultWallet in your application, you can use the reactive functionalities for transactions that the plugin exposes, as described in the next session.


## Transactions

Each Vue applications can handle its own transaction queue until transactions are actually sent to the arweave network. 

To access the transaction queue you can run the following (in your vue components): 

```javascript
this.$arGetARTransactionsQueue
```

The transactions queue can also be accessed in templates with the following: 

```html
<template>
    <div>
    {{$arGetARTransactionsQueue}}
    </div>
</template>
```

To create a new transaction you can use the following (in your vue components):

```javascript
this.$arCreateTransaction({
  amount, // the amount to send. The default is 0, it must be specified only for wallet to wallet transactions
  receiverAddress, // the receiver address. The default is 'null', it must be specified only for wallet to wallet transactions
  sign, // if set to true the transaction will be signed after created. The default is false
  send, // if set to true the transaction will be send immediately after the creation. In this case transaction must be signed. The default is false
  data, // the data to attach to the transaction. Default is an empty string.
  tags, // the tags to attach to the transaction. It must be an array having this schema: [{name: 'tag1', value: 'valueTag1'}, {name: 'tag2', value: 'valueTag2'}]. The default is an empty array.
}
```

After having created a transaction, it will be pushed in the transactions queue of the application (that you can access, as said before, by means of `$arGetARTransactionsQueue()`).

Note that transactions pushed in the queue are objects containing the transaction created by the arweave.js library and an enriched transaction object created by VueArweave plugin. Thus, each transaction in the queue is an object having the following schema: 

```javascript
{
    arTransaction, // the original transaction created by the arweave.js library  
    enrichedTransaction, // an enriched transaction created by the VueArweave plugin
}
```

Enriched transactions contains all the info of the transaction created by the arweave.js plus the followings: 

```javascript
{
    senderAddress, // the sender address that is, in a nutshell, the owner of the transaction but already decoded as a arweave address
    feeAR, // the fee in AR 
    quantityAR, // the amount of the transaction in AR (if applicable)
    totalAR, // the total cost of the transaction in AR and, so, the fee plus the quantity
    transactionStatus, // the status of the transaction 
    dataString, // the data attached to the transaction already decoded as string
}
```

To add a transaction to the queue, you can use the following (in your vue components): 

```javascript
this.$arAddTransactionToQueue(transaction)
```

Note that the transaction send in the above command, must be an object transaction having the schema for transactions used by the VueArweave plugin. 

If you used the arweave.js library or some other method to create the transaction, you can enrich it and then push it to the queue by using the followings (in your vue components): 

```javascript
const enrichedTransaction = await $arUtilsEnrichTransaction(transaction);
this.$arAddTransactionToQueue({
    arTransaction: transaction,
    enrichedTransaction,
});
```

To remove a transaction from the queue, you can use the following (in your vue components):

```javascript
// the transaction must be a transaction following the schema of the VueArweave plugin as described before
this.$arRemoveTransactionFromQueue(transaction);
```

To sign and send a transaction, you have firstly to retrieve the transaction from the transactions queue, and then you can sign and send it by using `$arUtilsSignTransaction(transaction)` and `arUtilsSendTransaction(transaction)` as the followings (in your vue components): 

```javascript
const transaction = this.$arGetARTransactionsQueue[0];
// sign transaction 
await this.$arUtilsSignTransaction(transaction.arTransaction);
// send transaction
await this.$arUtilsSendTransaction(transaction.arTransaction);
```

After having sent a transaction, the status of the transaction will be reactively updated. Thus, of you run:

```javascript
const transaction = this.$arGetARTransactionsQueue[0];
transaction.enrichedTransaction.transactionStatus
```

You will receive a status code of '202' if the transaction was successfully sent to the network and it is awaiting for confirmation. 

When a transaction is sent the plugin will check its status every 10 seconds. When a transaction is confirmed the followings will happen: 

- the information of the wallet that sent the transaction are reactively updated (and so the balance in AR and the balance in winston of the wallets are updated);
- if the receiver is a wallet imported in the application, also the receiver wallet info are reactively updated;
- because a transaction can be sent only by the default wallet that the application was using at the moment of sending, when the transaction is confirmed also the reactive var containing the default wallet transactions ($arGetDefaultWalletTransactionHistory) history is updated;
- the interval that checks the transaction status every 10 seconds is cleared.

The plugin exposes also a set of utility methods, as described in the next section.

## Utility Methods

The VueArweave plugins exposes the following utility methods that can be used by all the vue components of your application:

```javascript
 // execute an arQL expression and return the results
 async $arUtilsExecArQL(arQLObject);
 
 // get all the transaction for the specified address that was already sent to the arweave network
 async $arUtilsGetTransactionsForAddress(arWeaveAddress);

 // given an owner returns an address
 async $arUtilsOwnerToAddress(owner);

 // translate winston to AR
 async $arUtilsWinstonToAR(winstonAmount);

 // given a transaction id, it returns the transaction. The transaction must have been already sent to the arweave network
 async $arUtilsGetTransaction(transactionId);

 // given a transaction id, return its status
 async $arUtilsGetTransactionStatus(transactionId);

 // given a transaction in the common arweave format, it returns an enriched transaction object
 async $arUtilsEnrichTransaction(transaction);

 // get the data attacched to a transaction
 async $arUtilsTransactionGetData(
     transactionId, // the id of the transaction
     decode, // if set to true decode the transaction data. The default is false
     string, // if set to true decode the transaction data into a string. The default is false
 );
```

## Full API

### Methods

#### $arGenerateWallet
Async function that generates a wallet and put it in the reactive wallets computed property (that can be accessed with `$arGetWallets`).
Returns the generated wallet.

```javascript
async $arGenerateWallet({
    name: 'the name for your wallet', // not mandatory, the default is 'null'
    isDefault: false||true, // not mandatory, the default is false. 
})
```

#### $arImportWallet
Async function that import a wallet and put it in the reactive wallets computed property (that can be accessed with `$arGetWallets`).
Returns the imported wallet.

```javascript
async $arImportWallet({
    name: 'the name for your wallet', // not mandatory, the default is 'null'
    isDefault: false||true, // not mandatory, the default is false. 
    jwt: 'your json wallet object', // mandatory, throws an error if the jwt is not specified
})
```

#### $arRemoveWallet
Remove a wallet from the reactive wallets computed property (that can be accessed with `$arGetWallets`).
Returns void.

```javascript
$arRemoveWallet(
    walletId // the id of the wallet that you want to remove (that is the unique id generated by the plugins for the wallet when you have generated or imported it)
)
```

#### $arSetDefaultWallet
Async function that set the default wallet that the application must use for transactions (the default wallet will then be available at `$arGetDefaultWallet`).
When a default wallet is set, also the reactive computed properties that stores the wallet transaction history is updated (you can access it at `$arGetDefaultWalletTransactionHistory`)
Returns voids.

```javascript
async $arSetDefaultWallet(
    walletId // the id of the wallet that you want to remove (that is the unique id generated by the plugins for the wallet when you have generated or imported it)
)
```

#### $arCreateTransaction
Async function to create a transaction (see the [Transactions](#transactions) section for more information and examples).
Return the created transaction.

```javascript
async $arUtilsSignTransaction({
    amount, // the amount to send, deafult is 0
    receiverAddress, // the receiver address, default is null
    sign, // true to sign the transaction at the moment of creation, default is false
    send, // true to send the transaction immediately after having created it, default is false. Transaction must be signed to be sent.
    data, // the date to send within the transaction, default is ''
    tags, // tags to send within the transaction, default is []. It must be an array having this schema: [{name: 'tag1', value: 'valueTag1'}, {name: 'tag2', value: 'valueTag2'}]
} ;
```

#### $arUtilsSignTransaction
Async function to sign a transaction (see the [Transactions](#transactions) section for more information and examples).
Return void.

```javascript
async $arUtilsSignTransaction(
    transaction, // the transaction object to sign);
```

#### $arUtilsSendTransaction
Async function to  send a transaction (see the [Transactions](#transactions) section for more information and examples).
Returns the result of posting the transaction to the arweave network.
```javascript
async $arUtilsSendTransaction(
    transaction // the transaction object to send
);
```

#### $arUtilsExecArQL
Async function that executex an arQL expression. 
Returns the result of the query.

```javascript
async $arUtilsExecArQL(
    arQLObject,  // the arQL object containing the arQL expression to send
);
```

#### $arUtilsGetTransactionsForAddress
Async function that gets all the transaction for the specified address that was already sent to the arweave network.
Returns all the transaction that involved the given address. 

```javascript
async $arUtilsGetTransactionsForAddress(
    arWeaveAddress, // the address for which you want to retrieve the transaction history
);
```

#### $arUtilsOwnerToAddres
Async function that given the owner of a transaction returns the address that sent the transaction.

```javascript
async $arUtilsOwnerToAddress(
    owner, // the owner of a transacion. 
);
```

#### $arUtilsWinstonToAR

Async function that translate winston to AR. 
Returns thhe winston amount in AR.

```javascript
async $arUtilsWinstonToAR(
    winstonAmount, // the winston amount to translate
);
```

#### $arUtilsGetTransaction
Async function that given a transaction id, it returns the transaction. The transaction must have been already sent to the arweave network. 
Returns the transaction object.

```javascript
async $arUtilsGetTransaction(
    transactionId, // the id of a transaction already sent to the arweave network
);
```

#### $arUtilsGetTransactionStatus

Async function that given a transaction id, return its status. The transaction must have been already sent to the arweave network. 
Returns an object containing the status of the transaction with its code and the status text.

```javascript
async $arUtilsGetTransactionStatus(
    transactionId, // the id of a transaction already sent to the arweave network
);
```
#### $arUtilsEnrichTransaction
Given a transaction in the common arweave format, it returns an enriched transaction object. 
Returns and object that contains an arTransaction object (the degfault arweave transaction object), and an enrichedTransaction object (that contains more useful info). See the [Transactions](#transactions) section for more information about enriched transaction objects.

```javascript
async $arUtilsEnrichTransaction(
    transaction, // a common ar transaction object to enrich
);
```
#### $arUtilsTransactionGetData
Get the data attacched to a transaction. 
Returns the data attached to the transaction.

```javascript
async $arUtilsTransactionGetData(
    transactionId, // the id of the transaction
    decode, // if set to true decode the transaction data. The default is false
    string, // if set to true decode the transaction data into a string. The default is false
);
```

### Computed Properties

#### $arGetWallets

A reactive computed property to get all the wallets that was generated or imported in the application. 
See the [Wallets](#wallets) section for more information and examples about wallets.

Access it in your vue components with: 

```javascript
this.$arGetWallets; 
```

Or in your templates with: 

```html
<template>
    <div>
    {{$arGetWallets}}
    </div>
</template>
```
#### $arGetDefaultWallet

A reactive computed property to get the default wallet that the vue application is currently using. 
See the [Wallets](#wallets) section for more information and examples about the default wallet.

Access it in your vue components with: 

```javascript
this.$arGetDefaultWallet; 
```

Or in your templates with: 

```html
<template>
    <div>
    {{$arGetDefaultWallet}}
    </div>
</template>
```

#### $arGetARTransactionsQueue

A reactive computed property to get the transactions queue that the vue application is currently handling. 
See the [Transactions](#transactions) section for more information and examples about the transactions queue.

Access it in your vue components with: 

```javascript
this.$arGetARTransactionsQueue; 
```

Or in your templates with: 

```html
<template>
    <div>
    {{$arGetARTransactionsQueue}}
    </div>
</template>
```
#### $arGetDefaultWalletTransactionHistory

A reactive computed property to get the transactions history of the default wallet that the application is currently using. 
See the [Transactions](#transactions) and the [Wallets](#wallets) sections for more information and examples about the transactions history of the default address.

Access it in your vue components with: 

```javascript
this.$arGetDefaultWalletTransactionHistory; 
```

Or in your templates with: 

```html
<template>
    <div>
    {{$arGetDefaultWalletTransactionHistory}}
    </div>
</template>
```

## Compile and minify for production

To build the package from the source code, clone this repository then run the followings

```bash
npm install
npm run build
```

## Test

The package does not still have unit testing. However, it supplies a test application (with UI) that you can use to test on the fly the functionalities of the plugin. To start the test application, clone this repository and run the followings: 

```bash
npm install
npm run test
```
