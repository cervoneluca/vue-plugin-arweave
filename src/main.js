import Arweave from 'arweave/web';
import { uniqueId } from 'lodash';

const VueArweave = {
  install(Vue, options) {
    const vue = Vue;

    // add the arweave object to the vue instance
    vue.$arweave = {};

    if (options && options.network) {
      vue.set(vue.$arweave, 'network', options.network);
    } else {
      vue.set(vue.$arweave, 'network', {
        host: 'arweave.net',
        port: 443,
        protocol: 'https',
        timeout: 20000,
        logging: false,
      });
      // vue.set(vue.$arweave, 'network', {});
    }

    const store = vue.observable({
      arWallets: [],
      arDefaultWallet: null,
      arDefaultWalletTransactionHistory: null,
      arARTransactionsQueue: [],
    });

    vue.mixin({
      beforeCreate() {
        if (!vue.$arweave.node) {
          const arweaveNode = Arweave.init(vue.$arweave.network);
          vue.set(vue.$arweave, 'node', arweaveNode);
        }
      },
      methods: {
        async $arGenerateWallet({
          name = null,
          isDefault = false,
        } = {}) {
          const w = await vue.$arweave.node.wallets.generate();
          const wall = {
            id: uniqueId(),
            address: await Vue.$arweave.node.wallets.jwkToAddress(w),
            type: 'generated',
            name,
            isDefault,
            balanceAR: 0,
            balanceWinston: 0,
            jwk: w,
          };
          store.arWallets.push(wall);
          if (store.arWallets.length === 1 || isDefault) {
            this.$arSetDefaultWallet(wall.id);
          }
          return wall;
        },
        async $arImportWallet({
          name = null,
          isDefault = false,
          jwk,
        }) {
          if (!jwk) {
            throw new Error('"$arImportWallet": You must pass a Json Wallet Key in the jwk option');
          }
          store.arWallets.forEach((w) => {
            if (w.jwk === jwk) {
              throw new Error('"$arImportWallet": You cannot import the same wallet two times');
            }
          });
          const wall = {
            id: uniqueId(),
            address: await Vue.$arweave.node.wallets.jwkToAddress(jwk),
            type: 'imported',
            name,
            isDefault,
            balanceAR: 0,
            balanceWinston: 0,
            jwk,
          };
          store.arWallets.push(wall);
          if (store.arWallets.length === 1 || isDefault) {
            this.$arSetDefaultWallet(wall.id);
          }
          // get the wallet balance
          wall.balanceWinston = await vue.$arweave.node.wallets.getBalance(
            wall.address,
          );
          wall.balanceAR = await vue.$arweave.node.ar.winstonToAr(wall.balanceWinston);
          return wall;
        },
        async $arSetDefaultWallet(walletId) {
          store.arWallets.forEach((w) => {
            if (w.id === walletId) {
              // eslint-disable-next-line no-param-reassign
              w.isDefault = true;
              store.arDefaultWallet = w;
            } else {
              // eslint-disable-next-line no-param-reassign
              w.isDefault = false;
            }
          });
          // get all the transactions made by the default wallet
          store.arDefaultWalletTransactionHistory = await this.$arUtilsGetTransactionsForAddress(
            store.arDefaultWallet.address,
          );
        },
        $arRemoveWallet(walletId) {
          // cycle among wallets
          store.arWallets.forEach((w) => {
            if (w.id === walletId) {
              const { isDefault } = w;
              const i = store.arWallets.indexOf(w);
              store.arWallets.splice(i, 1);
              if (isDefault && store.arWallets.length > 0) {
                this.$arSetDefaultWallet(store.arWallets[0].id);
              }
            }
          });
        },
        async $arCreateTransaction({
          amount = 0,
          receiverAddress = null,
          sign = false,
          send = false,
          data = '',
          tags = [],
        } = {}) {
          if (!this.$arGetDefaultWallet) {
            throw new Error('"$arCreateTransaction:"  To create a transaction you must set an arweave default wallet');
          }
          if (!receiverAddress && data === '') {
            throw new Error('"$arCreateTransaction:"  To create a transaction you must pass an arweave receiver address or some data');
          }
          if (receiverAddress && amount === 0) {
            throw new Error('"$arCreateTransaction:"  To create a transaction You must specify the AR amount to send');
          }
          const tr = {};
          if (receiverAddress) {
            tr.target = receiverAddress;
            tr.quantity = await vue.$arweave.node.ar.arToWinston(amount);
          }
          if (data !== '') {
            tr.data = data;
          }
          const transaction = await vue.$arweave.node.createTransaction(
            tr,
            this.$arGetDefaultWallet.jwk,
          );
          if (tags.length > 0) {
            tags.forEach((t) => {
              transaction.addTag(t.name, t.value);
            });
          }
          if (sign) {
            await this.$arUtilsSignTransaction(transaction);
          }
          if (send) {
            const enrichedTrToSend = {
              arTransaction: transaction,
              enrichedTransaction: await this.$arUtilsEnrichTransaction(transaction),
            };
            await this.$arAddTransactionToQueue(enrichedTrToSend);
            const results = this.$arUtilsSendTransaction(transaction);
            return results;
          }
          // add the transaction to the queue and return the transaction
          const enrichedTr = {
            arTransaction: transaction,
            enrichedTransaction: await this.$arUtilsEnrichTransaction(transaction),
          };
          await this.$arAddTransactionToQueue(enrichedTr);
          return enrichedTr;
        },
        async $arAddTransactionToQueue(transaction) {
          store.arARTransactionsQueue.push(transaction);
        },
        async $arRemoveTransactionFromQueue(transaction) {
          const trIndex = store.arARTransactionsQueue.indexOf(transaction);
          store.arARTransactionsQueue.splice(trIndex, 1);
        },
        async $arUtilsExecArQL(expression) {
          const result = await vue.$arweave.node.arql(expression);
          return result;
        },
        async $arUtilsGetTransactionsForAddress(address) {
          const arql = {
            op: 'or',
            expr1: {
              op: 'equals',
              expr1: 'from',
              expr2: address,
            },
            expr2: {
              op: 'equals',
              expr1: 'to',
              expr2: address,
            },
          };
          const transactions = await this.$arUtilsExecArQL(arql);
          const transactionsHistory = [];
          transactions.forEach(async (tId) => {
            try {
              const tr = await this.$arUtilsGetTransaction(tId);
              return transactionsHistory.push({
                arTransaction: tr,
                enrichedTransaction: await this.$arUtilsEnrichTransaction(tr),
              });
            } catch (err) {
              return err;
            }
          });
          return transactionsHistory;
        },
        async $arUtilsOwnerToAddress(owner) {
          const address = await vue.$arweave.node.wallets.ownerToAddress(owner);
          return address;
        },
        async $arUtilsWinstonToAR(winston) {
          const ar = await vue.$arweave.node.ar.winstonToAr(winston);
          return ar;
        },
        async $arUtilsGetTransaction(transactionId) {
          const transaction = await vue.$arweave.node.transactions.get(transactionId);
          return transaction;
        },
        async $arUtilsGetTransactionStatus(transactionId) {
          const transactionStatus = await vue.$arweave.node.transactions.getStatus(transactionId);
          return transactionStatus;
        },
        async $arUtilsEnrichTransaction(transaction) {
          const transactionCopy = { ...transaction };
          const senderAddress = await this.$arUtilsOwnerToAddress(transaction.owner);
          const feeAR = await this.$arUtilsWinstonToAR(transaction.reward);
          const quantityAR = (transaction.quantity)
            ? await this.$arUtilsWinstonToAR(transaction.quantity) : 0.00;
          const totalAR = Number(feeAR) + Number(quantityAR);
          const transactionStatus = await this.$arUtilsGetTransactionStatus(transactionCopy.id);
          let dataString = '';
          try {
            dataString = await transaction.get('data', { decode: true, string: true });
          } catch (e) {
            dataString = await transaction.get('data', { decode: false, string: false });
          }
          transactionCopy.senderAddress = senderAddress;
          transactionCopy.feeAR = feeAR;
          transactionCopy.quantityAR = quantityAR;
          transactionCopy.totalAR = totalAR;
          transactionCopy.transactionStatus = transactionStatus;
          transactionCopy.dataString = dataString;
          return transactionCopy;
        },
        async $arUtilsSignTransaction(transaction) {
          await vue.$arweave.node.transactions.sign(transaction, this.$arGetDefaultWallet.jwk);
        },
        async $arUtilsSendTransaction(transaction) {
          const result = await vue.$arweave.node.transactions.post(transaction);
          // update the transaction status in the queue
          // get the transaction in the queue
          this.$arGetARTransactionsQueue.forEach(async (tx) => {
            if (JSON.stringify(tx.arTransaction) === JSON.stringify(transaction)) {
              const newStatus = await this.$arUtilsGetTransactionStatus(transaction.id);
              // eslint-disable-next-line no-param-reassign
              tx.enrichedTransaction.transactionStatus = newStatus;

              // set an interval to check if the transaction is processed
              // when the transaction is processed remove it from the queue and
              // refresh the transactions history and the wallets info
              const int = setInterval(async () => {
                const checkTransaction = await this.$arUtilsGetTransactionStatus(transaction.id);
                if (checkTransaction.status === 200) {
                  // remove the transaction from the queue
                  // const txIndex = this.$arGetARTransactionsQueue.indexOf(tx);
                  // this.$arGetARTransactionsQueue.splice(txIndex, 1);
                  // eslint-disable-next-line no-param-reassign
                  tx.enrichedTransaction.transactionStatus = checkTransaction;

                  // get the transaction's sender and receiver
                  const txSender = await this.$arUtilsOwnerToAddress(transaction.owner);
                  const txReceiver = transaction.target;

                  // update the transactions history if the wallet that sent the transaction
                  // was the default one
                  if (txSender === this.$arGetDefaultWallet.address
                    || txReceiver === this.$arGetDefaultWallet.address) {
                    store.arDefaultWalletTransactionHistory = [];
                    store.arDefaultWalletTransactionHistory = await
                    this.$arUtilsGetTransactionsForAddress(
                      store.arDefaultWallet.address,
                    );
                  }

                  // update wallets info
                  this.$arGetWallets.forEach(async (w) => {
                    if (w.address === txSender || w.address === txReceiver) {
                      // get the wallet balance
                      // eslint-disable-next-line no-param-reassign
                      w.balanceWinston = await vue.$arweave.node.wallets.getBalance(
                        w.address,
                      );
                      // eslint-disable-next-line no-param-reassign
                      w.balanceAR = await this.$arUtilsWinstonToAR(w.balanceWinston);
                    }
                  });

                  clearInterval(int);
                }
              }, 10000);
            }
          });
          return result;
        },
        async $arUtilsTransactionGetData(transactionId, decode = false, string = false) {
          const txData = await vue.$arweave.node.transactions.getData(transactionId, {
            decode,
            string,
          });
          return txData;
        },
      },
      computed: {
        $arGetWallets() {
          return store.arWallets;
        },
        $arGetDefaultWallet() {
          return store.arDefaultWallet;
        },
        $arGetARTransactionsQueue() {
          return store.arARTransactionsQueue;
        },
        $arGetDefaultWalletTransactionHistory() {
          return store.arDefaultWalletTransactionHistory;
        },
      },
    });
  },
};

export default VueArweave;
